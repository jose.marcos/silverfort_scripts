# Powershell script to create users
# from https://activedirectorypro.com/create-bulk-users-active-directory/
#
# Import active directory module for running AD cmdlets
# run it from a computer that has RSAT tools installed or the AD role
#
# modified by jmarcos
#
#
Import-Module activedirectory
  
# Store the data from users
$Domain = Read-Host -Prompt "Enter domain name"
$CSVFile = Read-Host -Prompt "Enter users information [csv file] "
$ADUsers = Import-csv $CSVFile

# Loop through each row containing user details in the CSV file 
foreach ($User in $ADUsers)
{
    # Read user data from each field in each row and assign the data to a 
    # variable as below

    $Username 	= $User.username
    $Password 	= $User.password
    $Firstname 	= $User.firstname
    $Lastname 	= $User.lastname
    $OU 		= $User.ou 
#    $email      = $User.email
#    $streetaddress = $User.streetaddress
#    $city       = $User.city
#    $zipcode    = $User.zipcode
#    $state      = $User.state
#    $country    = $User.country
#    $telephone  = $User.telephone
#    $jobtitle   = $User.jobtitle
#    $company    = $User.company
#    $department = $User.department
#    $Password = $User.Password


    #Check to see if the user already exists in AD
    if (Get-ADUser -F {SamAccountName -eq $Username})
    {
      # If user does exist, give a warning
      Write-Warning "A user account with username $Username already exist in Active Directory."
    }
    else
    {
    # If User does not exist then proceed to create the new user account
    
    # Account will be created in the OU provided by the $OU variable read 
    # from the CSV file
    #
    New-ADUser `
        -SamAccountName $Username `
        -UserPrincipalName "$Username@$Domain" `
        -Name "$Firstname $Lastname" `
        -GivenName $Firstname `
        -Surname $Lastname `
        -Enabled $True `
        -DisplayName "$Firstname $Lastname" `
        -Path $OU `
        -AccountPassword (convertto-securestring $Password -AsPlainText -Force) `
        -PasswordNeverExpires $False # bolean $True or $Falsea `
        -ChangePasswordAtLogon $False # bolean $True or $False `
        -AccountExpirationDate "12/31/2021 12:00:00"
    
#        -City $city `
#        -Company $company `
#        -State $state `
#        -StreetAddress $streetaddress `
#        -OfficePhone $telephone `
#        -EmailAddress $email `
#        -Title $jobtitle `
#        -Department $department `

#    Add-ADGroupMember -Identity SFUsers -Members $Username
        
    }
}
