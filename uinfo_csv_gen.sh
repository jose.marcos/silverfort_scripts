#!/bin/bash
#

usage="\n
[E] usage: uname_csv_gen.sh <user_name_file> \n
[I] <user_name_file> - arquivo com a lista de usuarios\n
[I] no formato: <first name> <last_name> \n
"

[[ -z $1 ]] && echo -e $usage &&  exit 1

# verificando se o arquivo userinfo.csv existe
[ -f userinfo.csv ] && { echo "[I] apagando o arquivo userinfo.csv";\
                         rm -rf ./userinfo.csv; }

ou_default="CN=Users,DC=actarlab,DC=localnet"

echo -e "
Entre com a OU do Active Directory
ou pressione ENTER para a OU default
default: $ou_default
"
read  -p "OU: " _ou

[[ -z $_ou ]] && _ou=$ou_default && echo -e "[I] usando a OU default\n"


IFS=$'\n'

pgen () {
  # função para gerar passwords, 12 caracteres
  while true
  do
     _p=`cat /dev/urandom \
         |tr -dc '1234567890!@#$%qwertQWERTasdfgASDFGzxcvbZXCVB'|head -c 20`

     # se a password gerada não tiver os simbolos listados abaixo ela será
     # ignorada
     if ! [[ $_p =~ [!@#$%] ]]; then continue;fi

     break
  done
}

echo "firstname,lastname,username,password,ou" >> userinfo.csv

for i in $(cat $1)
do
    # gerar password, o resultado da função está em $_p
    pgen 

    # formatar o nome do usuário e gerar o user_id
    _uid=`echo $i |awk '{ print $1","$2","tolower(substr($1,1,1))tolower($2) }'`

    # printar os usuários
    echo $_uid,$_p,'"'$_ou'"' >> userinfo.csv
done

[ -f userinfo.csv ] && echo -e "[I] arquivo userinfo.csv gerado.\n"

#firstname,middleInitial,lastname,username,email,streetaddress,city,zipcode,state,country,department,password,telephone,jobtitle,company,ou


